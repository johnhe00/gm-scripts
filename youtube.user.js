// ==UserScript==
// @name        YouTube Beautifier
// @namespace   Joose
// @description No more "What to watch". Minimalistic and clean interface.
// @require     http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js
// @include     http://www.youtube.com/*
// @include     https://www.youtube.com/*
// @version     0.9
// ==/UserScript==

if (window.yt) window.yt.setConfig({'SHARE_ON_VIDEO_END': false});

var toggleWatched = function() {
    $.map($(".feed-item-container"), function(e) {
        if (e.innerHTML.indexOf('<div class="watched-badge">') !== -1) $(e).fadeToggle();
    });
};

var keyHandler = function(event) {
    // CTRL + DELETE
    if (event.altKey == 0 && event.ctrlKey == 1 && event.which == 46) {
        toggleWatched();
        event.preventDefault();
    }
    // CTRL + DOWN
    if (event.altKey == 0 && event.ctrlKey == 1 && event.which == 40) {
        window.location.href = "https://www.youtube.com/feed/subscriptions/";
        event.preventDefault();
    }
    // CTRL + UP
    if (event.altKey == 0 && event.ctrlKey == 1 && event.which == 38) {
        scrollToPlayer();
        event.preventDefault();
    }
};

// Add Custom CSS
$(function() {
    var css = "";

    // Remove stupid stuff
    css += "#appbar-onebar-upload-group { display: none }";
    css += "#footer-container { display: none }";
    css += "#masthead-appbar-container { display: none }";
    css += "#sb-button-notify { display: none }";
    css += ".branded-page-v2-primary-col-header-container { display: none }";
    css += ".branded-page-v2-secondary-col { visibility: hidden }";
    css += ".feed-header { display: none }";

    css += "#masthead-positioner-height-offset { height: 50px }";
    css += "#masthead-search { margin-left: 150px }";
    css += ".content-snap-width-3 .flex-width-enabled.flex-width-enabled-snap.site-center-aligned .content-alignment { width: 850px }";

    $("<style>" + css + "</style>").appendTo("head");
});

// Add Personal Customization
$(function() {
    $("#masthead-search-term").attr("placeholder", "I like little penguins");
    $("#logo-container").attr("href", "https://www.youtube.com/feed/subscriptions/");
    $("#watch-description").removeClass("yt-uix-expander-collapsed");
});

$(document).keyup(keyHandler);
