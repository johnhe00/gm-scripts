// ==UserScript==
// @name        Fun HTML
// @namespace   Joose
// @description Editable HTML
// @include     http*
// @version     1.0
// ==/UserScript==

(function() {
    var toggle = function(event) {
        // Ctrl + Alt + E
        if (event.ctrlKey == 1 && event.altKey == 1 && event.keyCode == 69) {
            document.documentElement.contentEditable = !JSON.parse(document.documentElement.contentEditable);
            event.preventDefault();
            return false;
        }
    };

    addEventListener("keyup", toggle);
    document.documentElement.contentEditable = false;
})();
