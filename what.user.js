// ==UserScript==
// @name        What.CD
// @namespace   Joose
// @include     https://what.cd/*
// @version     1.0
// ==/UserScript==

(function() {
    var modSnatched = function(display) {
        var elements = document.querySelectorAll(".snatched_group a.show_torrents_link ");
        for (var i = 0; i < elements.length; i++) {
            elements[i].click();
        }

        if (elements.length <= 0) {
            elements = document.getElementsByClassName("snatched_torrent");
            for (var i = 0; i < elements.length; i++) {
                elements[i].style.display = display;
            }
        }
    };

    var keyHandler = function(event) {
        // Ctrl + DELETE
        if (event.ctrlKey == 1 && event.keyCode == 46) {
            event.preventDefault();
            modSnatched("none");
        }
        // Ctrl + INSERT
        if (event.ctrlKey == 1 && event.keyCode == 45) {
            event.preventDefault();
            modSnatched("");
        }
    };

    addEventListener("keyup", keyHandler);
})();
