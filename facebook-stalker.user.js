// ==UserScript==
// @name        Facebook Stalker
// @namespace   Joose
// @include     https://www.facebook.com/ajax/typeahead/search/bootstrap*
// @require     http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js
// @grant       GM_getValue
// @grant       GM_setValue
// @version     1.0
// ==/UserScript==

var USER_ID = document.cookie.match(/c_user=(\d+)/)[1];

var BLACKLIST = {};
BLACKLIST[USER_ID] = true;

var URL = "https://www.facebook.com/ajax/typeahead/search/bootstrap.php?__a=1&filter[0]=user&lazy=0&token=v7&stale_ok=0&viewer=" + USER_ID;
if (window.location.href.indexOf(USER_ID) == -1) window.location.replace(URL);

var CARD = $("<article class='card'><span class='place'></span><img src='' alt='' class='portrait'/><h2 class='name'><a class='path' target='_blank' href=''></a></h2><br/><br/><ul class='info-list'></ul><br/><p class='friendship'><a target='_blank' href=''>Friendship</a></p><p class='track'><a href='#'></a></p></article>");

var POINT = $("<li class='bullet'><span class='heading'></span>: <span class='info'></span></li>");

var makeBullet = function(heading, info) {
    var el = POINT.clone();
    el.find(".heading").text(heading);
    el.find(".info").text(info);
    return el;
};

var makeCard = function (friend, i) {
    var el = CARD.clone();
    var name = friend.names[0];

    var uid = String(friend.uid);
    var index = String(friend.index * -1);

    if (index == "0") el.addClass("hidden");
    el.find(".place").text(i + ". ");
    el.find("img.portrait").attr("alt", name);
    // friend.photo is broken
    el.find("img.portrait").attr("src", "http://placehold.it/50x50");
    el.find(".path").text(name);
    el.find(".path").attr("href", friend.path);

    el.find(".info-list").append(makeBullet("Index", index));
    el.find(".info-list").append(makeBullet("Alias", friend.alias));
    if (friend.category) el.find(".info-list").append(makeBullet("Category", friend.category));

    el.find(".friendship a").attr("href", "/profile.php?id=" + USER_ID + "&and=" + uid);
    el.find(".track a").data("uid", uid);
    el.find(".track a").data("index", index);

    return el;
};

$(function() {
    var css = "";
    css += "pre { display: none }";
    css += "* { margin: 0; padding: 0 }";
    css += "html, body { background: #113; color: #fff; margin: 0 auto; width: 512px }";
    css += "a, a:active { color: #69f; text-decoration: none }";
    css += "a:hover { color: #0ff }";
    css += ".title { margin-top: 25px; text-align: center }";
    css += ".portrait { border: 1px solid #111; height: 50px; left: 25px; position: absolute; top: 25px; width: 50px }";
    css += ".place { font-size: 24px; line-height: 50px }";
    css += ".name { display: inline-block }";
    css += ".card { background: #047; border-radius: 10px; margin: 25px 0; padding: 25px; position: relative; text-align: center }";
    css += ".info-list { margin: 0 auto; text-align: left; width: 450px }";
    css += ".bullet { list-style-type: none }";
    css += ".heading { font-weight: bold }";
    css += ".friendship { text-align: center }";
    css += ".hidden { display: none }";
    $("<style>" + css + "</style>").appendTo("head");
});

$(function() {
    document.title = "Facebook Stalker List";

    var friends = $("pre").html();
    friends = friends.substring(friends.indexOf('{'));
    friends = $.parseJSON(friends).payload.entries;
    unsafeWindow.victims = cloneInto(friends, unsafeWindow);

    console.log(friends.length + " victims identified");
    $("body").append("<h1 class='title'>Facebook Stalk List</h1><section class='stalk-list'></section>");
    var index = 1;
    for(var i = 0; i < friends.length; i++) {
        var friend = friends[i];
        if (BLACKLIST[String(friend.uid)]) continue;
        $(".stalk-list").append(makeCard(friend, index));
        index++;
    }
});
