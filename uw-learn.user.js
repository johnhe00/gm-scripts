// ==UserScript==
// @name        UW Learn Beautifier
// @namespace   Joose
// @include     https://learn.uwaterloo.ca/*
// @version     1
// ==/UserScript==

(function() {
    var styleElement = document.createElement('style');
    styleElement.type = 'text/css';
    document.getElementsByTagName('head')[0].appendChild(styleElement);

    var css = ".d2l-max-width,.d2l-page-bg{max-width:100%}";
    css += ".d2l-background-global,.d2l-background-left,.d2l-background-right{height:auto}";

    styleElement.appendChild(document.createTextNode(css));
})();

