// ==UserScript==
// @name        Blank Page Notepad
// @namespace   Joose
// @description Makes a blank page more useful.
// @grant       GM_setValue
// @grant       GM_getValue
// @include     about:blank
// @version     1.1
// ==/UserScript==

(function() {
    var fade = function(e) {
        var op = 1;
        e.style.display = "block";
        e.style.opacity = op;
        var timer = setInterval(function() {
            if (op <= 0) {
                clearInterval(timer);
                e.style.display = "none";
            }
            op -= 0.1;
            e.style.opacity = op;
        }, 50);
    };

    var favicon = document.createElement("link");
    favicon.rel = "icon";
    favicon.href = "http://john-he.com/imgs/favicon.ico";
    document.head.appendChild(favicon);

    var removeAllAttrs = function(element) {
        for (var i = element.attributes.length; i > 0; i++)
            element.removeAttributeNode(element.attributes[i]);
    };
    removeAllAttrs(document.documentElement);
    removeAllAttrs(document.body)

    var text = document.createElement("textarea");
    text.style.width = "100%";
    text.style.height = "100%";
    text.style.padding = "5px";
    text.style.margin = "-3px auto 0";
    text.style.resize = "none";
    text.style.fontFamily = '"Helvetica Neue", Helvetica, Arial, sans-serif';
    text.style.fontSize = "20px";
    text.style.background = "#eee";
    text.placeholder = " Write on me...";

    var modal = document.createElement("p");
    modal.style.position = "fixed";
    modal.style.width = "100px";
    modal.style.height = "26px";
    modal.style.lineHeight = "26px";
    modal.style.margin = "-13px 0 0 -50px";
    modal.style.left = "50%";
    modal.style.top = "50%";
    modal.style.opacity = "0";
    modal.style.display = "none";
    modal.style.textAlign = "center";

    var save = document.createElement("a");
    save.href="#";
    save.innerHTML = "Save";
    save.style.position = "fixed";
    save.style.right = 0;
    save.style.top = "25px";
    save.style.width = "50px";
    save.style.height = "25px";
    save.style.background = "#ccc";
    save.style.textAlign = "center";
    save.style.margin = "5px 5px";
    save.style.lineHeight = "25px";
    save.onclick = function() {
        GM_setValue("text", text.value);
        modal.innerHTML = "Saved";
        fade(modal);
    };

    var load = document.createElement("a");
    load.href="#";
    load.innerHTML = "Load";
    load.style.position = "fixed";
    load.style.right = 0;
    load.style.width = "50px";
    load.style.height = "25px";
    load.style.background = "#ccc";
    load.style.textAlign = "center";
    load.style.margin = "5px 5px";
    load.style.lineHeight = "25px";
    load.onclick = function() {
        text.value = GM_getValue("text", "");
        modal.innerHTML = "Loaded";
        fade(modal);
    };

    document.title = "John's Notepad";
    document.body.innerHTML = "";
    document.body.style.margin = "0";
    document.body.style.padding = "0";
    document.body.appendChild(text);
    document.body.appendChild(save);
    document.body.appendChild(load);
    document.body.appendChild(modal);
})();

