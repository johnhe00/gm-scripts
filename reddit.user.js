// ==UserScript==
// @name        Reddit Beautifier
// @namespace   Joose
// @include     http://www.reddit.com/*
// @version     1
// ==/UserScript==

(function() {
    var styleElement = document.createElement('style');
    styleElement.type = 'text/css';
    document.getElementsByTagName('head')[0].appendChild(styleElement);

    var css = "div.unvoted { color: black !important }";

    styleElement.appendChild(document.createTextNode(css));
})();
