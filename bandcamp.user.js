// ==UserScript==
// @name        Bandcamp Thief
// @namespace   Joose
// @description Add real download links for songs.
// @include     http://*.bandcamp.com/album/*
// @include     http://*.bandcamp.com/track/*
// @include     http://*.bandcamp.com/*
// @version     1.0
// @run-at document-end
// ==/UserScript==

var bandcamp = function() {
    var create_link = function(url, text) {
        var link = document.createElement("a");
        link.href = url;
        link.innerHTML = text;
        return link;
    };

    var create_span = function(text) {
        var span = document.createElement("span");
        span.innerHTML = text;
        return span;
    };

    var track_handler = function(track) {
        var url = track.file["mp3-128"];
        var title = $("h2.trackTitle")[0]; // Should only be one
        var link = create_link(url, title.innerHTML);
        title.innerHTML = "";
        title.appendChild(link);
    };

    var album_handler = function(tracks) {
        var rows = $("#track_table tr");
        if(!rows || rows.length != tracks.length) {
            return;
        }

        for(var i = 0; i < rows.length; i++) {
            var row = rows[i];
            row.removeChild(row.querySelector(".track-number-col"));
            row.removeChild(row.querySelector(".info-col"));

            var dlink = row.querySelector("div.dl_link");
            dlink.innerHTML = "";
            dlink.appendChild(create_link(tracks[i].file["mp3-128"], "download"));

            var time = row.querySelector("span.time").innerHTML;
            var title = row.querySelector("div.title");
            title.innerHTML = tracks[i].track_num < 10 ? "&nbsp;" : "";
            title.innerHTML += tracks[i].track_num + ". ";
            title.innerHTML += tracks[i].title + "&nbsp;&nbsp;&nbsp;" + time;
        }
    };

    if (!window.TralbumData || !window.TralbumData.trackinfo) {
        console.log("No track data found.");
        return;
    }

    if (window.location.pathname.search("/album/") != -1) {
        console.log("Album Handler");
        album_handler(window.TralbumData.trackinfo);
    }
    else if (window.location.pathname.search("/track/") != -1) {
        console.log("Track Handler");
        track_handler(window.TralbumData.trackinfo[0]);
    }
    else {
        console.log("Album Handler");
        album_handler(window.TralbumData.trackinfo);
    }
};

var script = document.createElement("script");
script.textContent = "(" + bandcamp + ")();";
document.documentElement.appendChild(script);

